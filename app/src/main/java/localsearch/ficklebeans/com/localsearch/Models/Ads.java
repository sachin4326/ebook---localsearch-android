package localsearch.ficklebeans.com.localsearch.Models;

public class Ads {

    private String adId;
    private String adName;
    private String adCategory;
    private int adPriority;
    private String adImage;
    private String  adDescription;
    private String adOwner;

    public Ads(String adId, String adName, String adCategory, int adPriority, String adImage, String adDescription, String adOwner) {
        this.adId = adId;
        this.adName = adName;
        this.adCategory = adCategory;
        this.adPriority = adPriority;
        this.adImage = adImage;
        this.adDescription = adDescription;
        this.adOwner = adOwner;
    }

    public Ads() {
    }

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public String getAdCategory() {
        return adCategory;
    }

    public void setAdCategory(String adCategory) {
        this.adCategory = adCategory;
    }

    public int getAdPriority() {
        return adPriority;
    }

    public void setAdPriority(int adPriority) {
        this.adPriority = adPriority;
    }

    public String getAdImage() {
        return adImage;
    }

    public void setAdImage(String adImage) {
        this.adImage = adImage;
    }

    public String getAdDescription() {
        return adDescription;
    }

    public void setAdDescription(String adDescription) {
        this.adDescription = adDescription;
    }

    public String getAdOwner() {
        return adOwner;
    }

    public void setAdOwner(String adOwner) {
        this.adOwner = adOwner;
    }
}
