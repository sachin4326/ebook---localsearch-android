package localsearch.ficklebeans.com.localsearch.Fragments;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.supercharge.shimmerlayout.ShimmerLayout;
import localsearch.ficklebeans.com.localsearch.Adapter.CategoryAdapter;
import localsearch.ficklebeans.com.localsearch.Adapter.HalfAdsAdapter;
import localsearch.ficklebeans.com.localsearch.Adapter.ItemsAdapter;
import localsearch.ficklebeans.com.localsearch.Adapter.MainAdsAdapter;
import localsearch.ficklebeans.com.localsearch.Adapter.QuadAdsAdapter;
import localsearch.ficklebeans.com.localsearch.Models.Ads;
import localsearch.ficklebeans.com.localsearch.Models.Category;
import localsearch.ficklebeans.com.localsearch.Models.Item;
import localsearch.ficklebeans.com.localsearch.R;
import localsearch.ficklebeans.com.localsearch.Utils.GridSpacingItemDecoration;
import localsearch.ficklebeans.com.localsearch.Utils.RecyclerTouchListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ITEMS_PATH = "items";
    private static final String ADS_PATH = "ads";
    private static final String CATEGORY_PATH ="category";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private RecyclerView recyclerView;
    private ItemsAdapter adapter;
    private ItemsAdapter featuredAdapter;
    private List<Item> itemList;
    private RecyclerView adsRecyclerView;
    private RecyclerView halfAdsRecyclerView;
    private RecyclerView quadAdsRecyclerView;
    private RecyclerView categoryRecyclerView;
    private MainAdsAdapter adsAdapter;
    private HalfAdsAdapter halfadsAdapter;
    private QuadAdsAdapter quadadsAdapter;
    private CategoryAdapter categoryAdapter;
    private List<Ads> adsList;
    private List<Ads> halfAdsList;
    private List<Ads> quadAdsList;
    private List<Item> featuredItemList;
    private List<Category> categoryList;


    private ShimmerLayout shimmerText;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private RecyclerView featuredItemsRecyclerView;


    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recentSearchRv);
        adsRecyclerView = (RecyclerView) view.findViewById(R.id.adsRv);
        categoryRecyclerView = (RecyclerView) view.findViewById(R.id.categoryRecyclerView);
        halfAdsRecyclerView = (RecyclerView) view.findViewById(R.id.halfAdsRecyclerView);
        quadAdsRecyclerView = (RecyclerView) view.findViewById(R.id.quadAdsRecyclerView);
        featuredItemsRecyclerView = (RecyclerView) view.findViewById(R.id.featuredRv);


        shimmerText = (ShimmerLayout) view.findViewById(R.id.shimmer_text);
        shimmerText.startShimmerAnimation();



        itemList = new ArrayList<>();
        featuredItemList = new ArrayList<>();
        adsList = new ArrayList<>();
        halfAdsList = new ArrayList<>();
        quadAdsList = new ArrayList<>();
        categoryList = new ArrayList<>();



        adapter = new ItemsAdapter(getContext(), itemList,getActivity());
        featuredAdapter = new ItemsAdapter(getContext(), featuredItemList,getActivity());
        adsAdapter = new MainAdsAdapter(getContext(), adsList,getActivity());
        halfadsAdapter = new HalfAdsAdapter(getContext(), halfAdsList,getActivity());
        quadadsAdapter = new QuadAdsAdapter(getContext(), quadAdsList,getActivity());
        categoryAdapter = new CategoryAdapter(getContext(), categoryList,getActivity());



        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setAdapter(adapter);

        featuredItemsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        featuredItemsRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        featuredItemsRecyclerView.setAdapter(featuredAdapter);


        adsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));
        adsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        adsRecyclerView.setAdapter(adsAdapter);


        halfAdsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false));
        halfAdsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        halfAdsRecyclerView.setAdapter(halfadsAdapter);

        quadAdsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4, GridLayoutManager.HORIZONTAL, false));
        quadAdsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        quadAdsRecyclerView.setAdapter(quadadsAdapter);




        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));
        categoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        categoryRecyclerView.setAdapter(categoryAdapter);

        

        loadItemsForServer(ITEMS_PATH);
        loadItemsForServer(ADS_PATH);
        loadItemsForServer(CATEGORY_PATH);


        return  view;
    }

    private void loadItemsForServer(String path) {
        shimmerText.startShimmerAnimation();
        mDatabase = FirebaseDatabase.getInstance().getReference().child(path);

        if(path==ITEMS_PATH){
            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    itemList.clear();
                    int count = 1;
                    for (DataSnapshot item : dataSnapshot.getChildren()) {



                        Item newItem = item.getValue(Item.class);

                        /*  if (newItem.getCategory().equalsIgnoreCase("chicken")) {*/

                        Item newItemAdd = new Item(
                                newItem.getItemName(), newItem.getItemId(), newItem.getItemImage(), newItem.getItemUrl(), newItem.getItemDescription(), newItem.getItemPrice(), newItem.getItemOwner(), newItem.getItemOwnerDescription(),newItem.getItemCategory()
                        );
                        itemList.add(newItemAdd);

                        if(count<5) {
                            featuredItemList.add(newItemAdd);
                        }

                        /*   Log.e("list", chickenItem.getCategory());*/
                        count++;

                    }

                    adapter.notifyDataSetChanged();
                    featuredAdapter.notifyDataSetChanged();
                    shimmerText.stopShimmerAnimation();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }


            });


        }
        else if(path==ADS_PATH){

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    adsList.clear();
                    halfAdsList.clear();
                    quadAdsList.clear();
                    for (DataSnapshot ad : dataSnapshot.getChildren()) {



                        Ads newItem = ad.getValue(Ads.class);
                        Ads newItemAdd = new Ads(
                                newItem.getAdId(), newItem.getAdName(), newItem.getAdCategory(), newItem.getAdPriority(), newItem.getAdImage(), newItem.getAdDescription(), newItem.getAdOwner()
                        );

                        if (newItem.getAdCategory()!=null && newItem.getAdCategory().equalsIgnoreCase("MAIN")) {


                            adsList.add(newItemAdd);
                        }else if(newItem.getAdCategory()!=null && newItem.getAdCategory().equalsIgnoreCase("HALF")){

                            halfAdsList.add(newItemAdd);



                        }else{

                            quadAdsList.add(newItemAdd);
                        }

                        /*   Log.e("list", chickenItem.getCategory());*/


                    }

                    adsAdapter.notifyDataSetChanged();
                    halfadsAdapter.notifyDataSetChanged();
                    quadadsAdapter.notifyDataSetChanged();
                    shimmerText.stopShimmerAnimation();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


else {
            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    categoryList.clear();
                    for (DataSnapshot ad : dataSnapshot.getChildren()) {



                        Category newItem = ad.getValue(Category.class);

                        /*  if (newItem.getCategory().equalsIgnoreCase("chicken")) {*/

                        Category newItemAdd = new Category(
                                newItem.getCategoryName(), newItem.getCategoryId(), newItem.getCategoryImage(), newItem.getCategoryUrl()
                        );
                        categoryList.add(newItemAdd);

                        /*   Log.e("list", chickenItem.getCategory());*/


                    }

                    categoryAdapter.notifyDataSetChanged();
                    shimmerText.stopShimmerAnimation();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }





    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
