package localsearch.ficklebeans.com.localsearch;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import localsearch.ficklebeans.com.localsearch.Models.Ads;
import localsearch.ficklebeans.com.localsearch.Models.Item;

public class DetailedItemActivity extends AppCompatActivity {


    String itemType = "";
    String itemImage = "";
    String key = "";
    String titleText = "";
    String categoryText = "";
    String descriptionText = "";
    String ownerText = "";
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_item);

        Intent i = getIntent();

        itemImage = i.getStringExtra("image");
        key = i.getStringExtra("key");
        itemType = i.getStringExtra("type");
        titleText = i.getStringExtra("title");
        descriptionText = i.getStringExtra("des");
        categoryText = i.getStringExtra("category");
        ownerText = i.getStringExtra("owner");




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle(titleText);
        ImageView image = (ImageView) findViewById(R.id.thumbnail);
        TextView title = (TextView) findViewById(R.id.title);
        TextView category = (TextView) findViewById(R.id.category);
        TextView description = (TextView) findViewById(R.id.description);
        TextView owner = (TextView) findViewById(R.id.owner);


        title.setText(titleText);
        category.setText(categoryText);
        description.setText(descriptionText);
        owner.setText(ownerText);


        Glide.with(this).load(itemImage).into(image);






    }





    /*public Ads getAdsDetailById(final String uid){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = mDatabase.child("ads").child(uid);
        Semaphore sem = new Semaphore(0);
        final Ads[] array = new Ads[1];
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot item: dataSnapshot.getChildren()) {
                    if (item.getKey()==uid)
                    {
                        Ads user= dataSnapshot.getValue(Ads.class);
                        array[0] = user;
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        try
        {
            sem.tryAcquire(10, TimeUnit.SECONDS);
        }
        catch (Exception ignored)
        {
        }
        return array[0];
    }

    public Item getItemDetailById(final String uid){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = mDatabase.child("items").child(uid);
        Semaphore sem = new Semaphore(0);
        final Item[] array = new Item[1];
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot item: dataSnapshot.getChildren()) {
                    if (item.getKey()==uid)
                    {
                        Item user= dataSnapshot.getValue(Item.class);
                        array[0] = user;
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        try
        {
            sem.tryAcquire(10, TimeUnit.SECONDS);
        }
        catch (Exception ignored)
        {
        }
        return array[0];
    }*/




}
