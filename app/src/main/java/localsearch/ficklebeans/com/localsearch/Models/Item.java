package localsearch.ficklebeans.com.localsearch.Models;

public class Item {

    private String itemName;
    private String itemId;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    private String itemImage;
    private String itemUrl;
    private String itemDescription;
    private String itemPrice;
    private String itemOwner;
    private String itemOwnerDescription;
private String itemCategory;

    public Item() {
    }

    public String getItemCategory() {
        return itemCategory;
    }


    public Item(String itemName, String itemCategory, String itemImage) {
        this.itemName = itemName;
        this.itemImage = itemImage;
        this.itemCategory = itemCategory;
    }

    public Item(String itemName, String itemCategory, String itemImage, String itemDescription) {
        this.itemName = itemName;
        this.itemImage = itemImage;
        this.itemCategory = itemCategory;
        this.itemDescription = itemDescription;
    }

    public Item(String itemName, String itemId, String itemImage, String itemUrl, String itemDescription, String itemPrice, String itemOwner, String itemOwnerDescription, String itemCategory) {
        this.itemName = itemName;
        this.itemId = itemId;
        this.itemImage = itemImage;
        this.itemUrl = itemUrl;
        this.itemDescription = itemDescription;
        this.itemPrice = itemPrice;
        this.itemOwner = itemOwner;
        this.itemOwnerDescription = itemOwnerDescription;
        this.itemCategory = itemCategory;
    }

    public void setItemCategory(String itemCategory) {
        this.itemCategory = itemCategory;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemOwner() {
        return itemOwner;
    }

    public void setItemOwner(String itemOwner) {
        this.itemOwner = itemOwner;
    }

    public String getItemOwnerDescription() {
        return itemOwnerDescription;
    }

    public void setItemOwnerDescription(String itemOwnerDescription) {
        this.itemOwnerDescription = itemOwnerDescription;
    }





}
