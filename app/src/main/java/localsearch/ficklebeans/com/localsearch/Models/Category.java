package localsearch.ficklebeans.com.localsearch.Models;

public class Category {

    private String categoryName;
    private  String categoryId;
    private String categoryImage;
    private String categoryUrl;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public Category(String categoryName, String categoryId, String categoryImage, String categoryUrl) {
        this.categoryName = categoryName;
        this.categoryId = categoryId;
        this.categoryImage = categoryImage;
        this.categoryUrl = categoryUrl;
    }

    public Category() {
    }
}
