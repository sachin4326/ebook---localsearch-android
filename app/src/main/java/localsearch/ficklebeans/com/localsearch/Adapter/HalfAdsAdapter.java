package localsearch.ficklebeans.com.localsearch.Adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import localsearch.ficklebeans.com.localsearch.DetailedItemActivity;
import localsearch.ficklebeans.com.localsearch.Models.Ads;
import localsearch.ficklebeans.com.localsearch.R;

public class HalfAdsAdapter extends RecyclerView.Adapter<HalfAdsAdapter.MyItemViewHolder> implements Filterable {

    private Context mContext;
    private List<Ads> itemList;
    private Activity rootActivity;


    public HalfAdsAdapter(Context mContext, List<Ads> itemList, Activity rootActivity) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.rootActivity = rootActivity;
    }

    @NonNull
    @Override
    public MyItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.hlf_ads_card, viewGroup, false);

        return new HalfAdsAdapter.MyItemViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyItemViewHolder holder, int position) {
        final Ads item = itemList.get(position);
        holder.title.setText(item.getAdName());
        holder.category.setText(item.getAdCategory());

        // loading album cover using Glide library
        Glide.with(mContext).load(item.getAdImage()).into(holder.thumbnail);



        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mContext, DetailedItemActivity.class);

                View sharedView = holder.thumbnail;
                String transitionName = mContext.getString(R.string.detailImageItem);

                i.putExtra("type","Ads");
                i.putExtra("key",item.getAdId());
                i.putExtra("image",item.getAdImage());
                i.putExtra("title",item.getAdName());
                i.putExtra("category",item.getAdOwner());
                i.putExtra("des",item.getAdDescription());

                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(rootActivity, sharedView, transitionName);
                mContext.startActivity(i, transitionActivityOptions.toBundle());

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public class MyItemViewHolder extends RecyclerView.ViewHolder {
        public TextView title, category;
        public ImageView thumbnail, overflow;

        public MyItemViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            category = (TextView) view.findViewById(R.id.category);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);

        }
    }







}
