package localsearch.ficklebeans.com.localsearch.Adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import localsearch.ficklebeans.com.localsearch.DetailedItemActivity;
import localsearch.ficklebeans.com.localsearch.Models.Item;
import localsearch.ficklebeans.com.localsearch.R;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.MyItemViewHolder> implements Filterable {

    private Context mContext;
    private List<Item> itemList;
    private Activity rootActivity;


    public ItemsAdapter(Context mContext, List<Item> itemList,Activity rootActivity) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.rootActivity = rootActivity;
    }

    @NonNull
    @Override
    public MyItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_card, viewGroup, false);

        return new ItemsAdapter.MyItemViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyItemViewHolder holder, int position) {
        final Item item = itemList.get(position);
        holder.title.setText(item.getItemName());
        holder.category.setText(item.getItemCategory());

        // loading album cover using Glide library
        Glide.with(mContext).load(item.getItemImage()).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mContext, DetailedItemActivity.class);

                View sharedView = holder.thumbnail;
                String transitionName = mContext.getString(R.string.detailImageItem);

                i.putExtra("type","Ads");
                i.putExtra("key",item.getItemId());
                i.putExtra("image",item.getItemImage());
                i.putExtra("title",item.getItemName());
                i.putExtra("category",item.getItemCategory());
                i.putExtra("des",item.getItemDescription());

                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(rootActivity, sharedView, transitionName);
                mContext.startActivity(i, transitionActivityOptions.toBundle());

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public class MyItemViewHolder extends RecyclerView.ViewHolder {
        public TextView title, category;
        public ImageView thumbnail, overflow;

        public MyItemViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            category = (TextView) view.findViewById(R.id.category);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            overflow = (ImageView) view.findViewById(R.id.overflow);
        }
    }




    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.item_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new ItemsAdapter.MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.item_share:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.item_more:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

}
