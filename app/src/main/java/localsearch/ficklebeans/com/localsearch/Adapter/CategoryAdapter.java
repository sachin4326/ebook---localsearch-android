package localsearch.ficklebeans.com.localsearch.Adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import localsearch.ficklebeans.com.localsearch.DetailedItemActivity;
import localsearch.ficklebeans.com.localsearch.Models.Ads;
import localsearch.ficklebeans.com.localsearch.Models.Category;
import localsearch.ficklebeans.com.localsearch.R;
import localsearch.ficklebeans.com.localsearch.SearchItemActivity;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyItemViewHolder> implements Filterable {

    private Context mContext;
    private List<Category> itemList;
    private Activity rootActivity;


    public CategoryAdapter(Context mContext, List<Category> itemList, Activity rootActivity) {
        this.mContext = mContext;
        this.itemList = itemList;
        this.rootActivity = rootActivity;
    }

    @NonNull
    @Override
    public MyItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.category_row_item, viewGroup, false);

        return new CategoryAdapter.MyItemViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyItemViewHolder holder, int position) {
        final Category item = itemList.get(position);
        holder.title.setText(item.getCategoryName());


        // loading album cover using Glide library
        Glide.with(mContext).load(item.getCategoryImage()).into(holder.thumbnail);



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mContext, SearchItemActivity.class);
                i.putExtra("category", item.getCategoryName());
                View sharedView = holder.itemView;
                String transitionName = "categoryName";

                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(rootActivity, sharedView, transitionName);
                mContext.startActivity(i, transitionActivityOptions.toBundle());

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public class MyItemViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail, overflow;

        public MyItemViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);

            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);

        }
    }







}
